from tp2 import *

# ----~~~ Tests Boiboites ~~~----
b = box()
b.add ("truc1")
b.add("truc2")
assert b.action_look() == "La boite contient : truc1, truc2"
assert "truc2" in b 
b.remove("truc1")
assert "truc1" not in b
assert b.action_look() == "La boite contient : truc2"
assert b.is_open()
b.close()
assert not b.is_open == False 
assert b.action_look() == "La boite est fermee."
b.open()
assert b.is_open()


# ----~~~ Tests About Things ~~~----

p=box()
t= thing(3)
y=thing(7)
assert t.volume == 3
p.set_capacity(5)
p.capacity == 5
assert p.assez_de_place(t)
assert not p.assez_de_place(y)
assert not p.action_add(y)
p.close()
assert not p.action_add(t)
p.open()
assert p.action_add(t)
assert p.capacity == 2
assert(t in p)
t.set_name('patate')
assert t.__repr__() == 'patate'
assert not t.has_name('panda')
assert t.has_name('patate')
assert not p.find('gse')
assert p.find('patate')
p.close()
assert not p.find('patate')
r=thing(5,'yolo')
assert r.has_name('yolo')
assert r.__repr__() == 'yolo'

# ----~~~ Tests about gitting commiting ang gitlabbing ~~~----

p.setColor('red')
assert p.color == 'red'
r.setColor('blue')
assert r.color = 'blue'
