class box (object):
    def __init__(self, is_open=True, capacity=None, color=None):
        self.__contents = []
        self.is_open=is_open
        self.capacity=capacity
        self.color = color
    
    @staticmethod
    def from_data(data):
        o = data.get("is_open", None)
        c = data.get("capacity", None)
        return box(o,c)
    
    def __repr__(self):
        if not self.is_open():
            return "La boite est fermée."
        else:
            return "<capacité : %s, %s>" % (self.capacity,self.__contents)
        
    
    def __contains__(self,machin):
        return (machin in self.__contents)
    
    def add(self, truc):
        self.__contents.append(truc)
    
    def remove(self,truc):
        if truc in self.__contents :
            self.__contents.remove(truc)
    
    # ----~~~ Ouvrir et fermer ~~~----
    def close(self):
        self.is_open=False
    def open(self):
        self.is_open=True
    def is_open(self):
        return self.is_open
    
    
    # ----~~~ Regarder ~~~----    
    def action_look(self):
        if self.is_open():
            res="La boite contient : "
            contenu=[]
            test=thing(0)
            for elem in self.__contents:
                if type(elem) == type(test):
                    if elem.name != None :
                        contenu.append(elem.name)
                    else:
                        contenu.append('un truc sans nom')
                else:
                    contenu.append(elem)
            res+=", ".join(contenu)
        else:
            res="La boite est fermee."
        return res
    
    # ----~~~ Capacite & Things ~~~----       
    def set_capacity(self,c):
        self.capacity=c
        
    def assez_de_place(self,thing):
        return self.capacity >= thing.volume
    
    def action_add(self, thing):
        if self.assez_de_place(thing) and self.is_open():
            self.add(thing)
            self.capacity-=thing.volume
            res=True
        else:
            res=False
        return res
    
    def find(self, nom):
        trouve=False
        if self.is_open():
            i=0
            test=thing(0)
            while not trouve and i < len(self.__contents):
                elem = self.__contents[i]
                if type(elem) == type(test):
                    if elem.has_name(nom):
                        trouve=True
                else:
                    if elem == nom:
                        trouve = True
                i+=1
        return trouve
        
    #  ----~~~ Gitlabbing ~~~----     
    def setColor(self, c):
        self.color=c
	

class thing(object):
    def __init__(self, taille, name=None, color = None):
        self.volume=taille
        self.name=name
        self.color = None
    
    def __repr__(self):
        return self.name
    
    def set_name (self, bidule):
        self.name=bidule
    
    
    def has_name(self, mot):
        return mot == self.name
        
        
    #  ----~~~ Gitlabbing ~~~----     
    def setColor(self, c):
        self.color=c
